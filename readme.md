# Memory game

from **The Web Game Developer's Cookbook** by Evan Burchard.

## Book info

pw had this book at one time. He may still have it somewhere.

### GitHub repo

https://github.com/EvanBurchard/jsarcade

### Amazon link

https://www.amazon.com/Web-Game-Developers-Cookbook-JavaScript/dp/0321898389/ref=sr_1_1?crid=2E3L6ISBJCV2M&keywords=web+developer%27s+game+cookbook&qid=1676773347&sprefix=web+developer%27s+game+cookbook%2Caps%2C232&sr=8-1
